# Start FROM Nvidia PyTorch image https://ngc.nvidia.com/catalog/containers/nvidia:pytorch
FROM nvcr.io/nvidia/pytorch:21.10-py3

# Install linux packages
RUN apt update && apt install -y zip htop screen libgl1-mesa-glx libglib2.0-0 libsm6 libxrender1 libxext6
#RUN apt install -y python3 python3-pip
ENV NVIDIA_VISIBLE_DEVICES=all
# Install python dependencies
COPY requirements.txt .
RUN python -m pip install --upgrade pip
# RUN pip uninstall -y nvidia-tensorboard nvidia-tensorboard-plugin-dlprof
RUN pip install --no-cache -r requirements.txt
# RUN pip install --no-cache -U torch torchvision numpy Pillow

# RUN pip install --no-cache torch==1.10.0+cu113 torchvision==0.11.1+cu113 -f https://download.pytorch.org/whl/cu11/torch_stable.html

# Create working directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
# Copy contents
COPY . /usr/src/app
RUN rm -rf data
RUN mkdir data
RUN python3 fetch_data.py --pem_file lincode_dataset_server
RUN sh test_inference.sh
