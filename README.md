# Model inference

## Getting started

This repo will contain only code pertaining to model inferencing. 
Any changes to the code here should be used only for model inference. 

## TODO:

- [ ] A config file containing model inference parameters like, classes.txt file etc needed to do inference
- [ ] The config file must also contain the location to upload the model inference results, graphs and other metrics required.

