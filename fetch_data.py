import paramiko
from tqdm import tqdm
import os
from argparse import ArgumentParser


def connect_to_server(config):
    """
    This function initializes a SSHClient by using the PEM.
    @param config: dictionary containing IP addr and username to connect to dataset server and downloading data
    """
    ssh_client = paramiko.SSHClient()
    ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh_client.connect(hostname=config['ip'], username=config['username'], password='', key_filename=config['pem_file'])
    download_data(ssh_client)


def download_data(ssh_client):
    """
    Uses the FTP protocol to download images using the ssh_client
    """
    stdin, stdout, stderr = ssh_client.exec_command(EXEC_CMD)
    files_list = stdout.readlines()
    ftp_client = ssh_client.open_sftp()
    print(f"\nCopying files from: {SOURCE_PATH} Destination: {DESTINATION_PATH}")
    for file in tqdm(files_list, desc='Downloading files'):
        ftp_client.get(SOURCE_PATH + file.split('\n')[0],
                       DESTINATION_PATH + file.split('\n')[0])

    ssh_client.close()


def make_dirs():
    """
    This function should be created to copy folders from manager IP to node.
    Currently this file is capable of copying just files.
    """
    pass


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('--username', type=str, help='Username for the node server', default='root', required=False)
    parser.add_argument('--pem_file', type=str, help='location of the pem file for the node.', required=False)
    parser.add_argument('--ip', type=str, help='IP address of the node', required=False)
    args = parser.parse_args()

    MANAGER_IP = '139.59.28.87'
    USERNAME = 'root'
    PASSWORD = ''
    PROJECT_NAME = 'TASL_data' + '/'
    DATA_FOLDER = 'data/'
    SOURCE_PATH = '/root/Dataset/' + PROJECT_NAME + DATA_FOLDER
    DESTINATION_PATH = os.getcwd() + '/data/'
    EXEC_CMD = 'cd Dataset/' + PROJECT_NAME + '/data && ls'
    args = args.__dict__
    config = {'ip': MANAGER_IP,
              'username': USERNAME,
              }

    if args['pem_file'] is not None:
        config['pem_file'] = args['pem_file']

    if not os.path.exists(os.getcwd() + '/data/'):
        print(f"Creating data directory")
        os.makedirs('data')

    if args['ip'] is None:
        connect_to_server(config)
    else:
        connect_to_server(args)
